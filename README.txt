DESCRIPTION

The website runs using Node.js as a backend server.  Express is a web application
framework which runs on Node.js, and it uses Jade as a server-side view templating engine.
The website uses Bootstrap for styling and responsiveness.  It uses Angular.js to create a
fluid single page application.

INSTALLATION

1. Install node.js from http://nodejs.org/download/
2. Ensure the nodejs directory from the installation is on your PATH.  You should be able to 
	execute the node and npm commands from a command prompt.
3. Open a command prompt to the root of the project.  (I.e. in the same directory as package.json.)
4. Type: npm install bower -g
	(Installs bower client-side dependency utility globally.)
5. Type: npm install
	(Installs project dependencies as specified in package.json.)
6. Type: bower install
	(Installs project client-side dependencies as specified in bower.json.)
7. Type: node server.js
	(This will start the server running on localhost at specified port.)
	
You can also visit the deployed website at justinlang.net.  A Linode VPS is running node and hosting
the website.
