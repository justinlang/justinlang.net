'use strict';

/**
* Index controller functions.
*/

exports.render = function(req, res) {
	res.render('index', {title: 'justinlang.net'});
};