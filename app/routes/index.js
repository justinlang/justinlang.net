'use strict';

/**
* Map controller functions to routes.
*/

module.exports = function(app) {
	var index = require('../controllers/index');
	app.get('/', index.render);
};