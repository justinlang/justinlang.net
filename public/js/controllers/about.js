'use strict';

var controllers = angular.module('net.justinlang.controllers');

controllers.controller('AboutController', ['$scope', function($scope) {
	$scope.header = 'About';

	var constructEmail = function() {
		var local = 'jjlang-cc';
		var domain = 'conestogac.on.ca';
		return local + '@' + domain;
	};

	$scope.email = constructEmail();
}]);