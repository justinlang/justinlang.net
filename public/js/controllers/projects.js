'use strict';

var controllers = angular.module('net.justinlang.controllers');

controllers.controller('ProjectsController', ['$scope', function($scope) {
	$scope.header = 'Projects';
}]);