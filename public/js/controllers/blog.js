'use strict';

var controllers = angular.module('net.justinlang.controllers');

controllers.controller('BlogController', ['$scope', function($scope) {
	$scope.header = 'Blog';
}]);