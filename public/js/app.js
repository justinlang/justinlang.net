'use strict';



angular.module('net.justinlang', ['ngRoute', 'ngAnimate', 'net.justinlang.controllers', 'net.justinlang.directives']);
angular.module('net.justinlang.controllers', []);
angular.module('net.justinlang.directives', []);

