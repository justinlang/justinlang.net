'use strict';

/**
* A custom navbar using Bootstrap and jQuery.
*/
var directives = angular.module('net.justinlang.directives');

directives.directive('jlNavbar', [function() {

	var directiveDefinitionObject = {
		restrict: 'E',
		replace: true,
		transclude: false,
		templateUrl: 'views/partials/navbar.html',
		link: function(scope, element, attrs) {
			// Close the collapsible menu when clicked
			$(element).on('click', '#navbar-collapse-collection a', function() {
				if ($('.navbar-toggle', element).is(':visible')) {
					$('.navbar-toggle').click();
				}
			});
		},
		controller: ['$scope', '$location', function($scope, $location) {
			$scope.isActive = function(viewLocation) {
				return viewLocation === $location.path();
			};
		}]
	};

	return directiveDefinitionObject;
}]);