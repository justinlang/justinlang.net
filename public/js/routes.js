'use strict';

angular.module('net.justinlang').config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/about', {
			templateUrl: 'views/about.html',
			controller: 'AboutController'
		})
		.when('/blog', {
			templateUrl: 'views/blog.html',
			controller: 'BlogController'
		})
		.when('/projects', {
			templateUrl: 'views/projects.html',
			controller: 'ProjectsController'
		})
		.otherwise({
			redirectTo: '/blog'
		});
}]);